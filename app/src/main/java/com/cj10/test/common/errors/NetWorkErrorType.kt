package com.cj10.test.common.errors

enum class NetworkErrorType {
    TIMEOUT, UNKNOWN
}