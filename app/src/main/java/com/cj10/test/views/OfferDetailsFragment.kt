package com.cj10.test.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.cj10.test.R
import com.cj10.test.common.states.ListViewState
import com.cj10.test.databinding.ItemDetailsLayoutBinding
import com.cj10.test.models.Item
import com.cj10.test.models.OfferDetails

class OfferDetailsFragment(private val viewModel: OfferItemDetailsViewModel, val item: Item): Fragment() {

    private lateinit var binding: ItemDetailsLayoutBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.item_details_layout, container, false)

        activity?.title = getString(R.string.details)

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.viewState.observe(this, Observer { handleStateChange(it) })
        viewModel.getOfferDetailsInformation(item.detailUrl)
    }

    private fun handleStateChange(state: ListViewState?) {
        when (state) {
            is ListViewState.Loading -> binding.isLoading = true
            is ListViewState.HasSingleData<*> -> {
                binding.isLoading = false
                binding.viewModel = viewModel
                viewModel.bind(
                    (state as ListViewState.HasSingleData<OfferDetails>).data,
                    context!!,
                    binding.imageViewProductDetails
                )
            }
            else -> binding.isLoading = false
        }
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_details, menu);
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_heart_item -> {
                Toast.makeText(context, "${getText(R.string.like)} ${getText(R.string.clicked)}", Toast.LENGTH_SHORT).show()
                true
            }
            R.id.menu_share_item -> {
                Toast.makeText(context, "${getText(R.string.share)} ${getText(R.string.clicked)}", Toast.LENGTH_SHORT).show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(
            offerItemDetailsViewModel: OfferItemDetailsViewModel,
            item: Item
        ): OfferDetailsFragment {
            return OfferDetailsFragment(offerItemDetailsViewModel, item)
        }
    }
}