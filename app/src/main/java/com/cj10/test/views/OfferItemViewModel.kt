package com.cj10.test.views

import android.content.Context
import android.widget.ImageView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bumptech.glide.Glide
import com.cj10.test.common.extensions.getLikesFormat
import com.cj10.test.common.interfaces.OnClickListener
import com.cj10.test.models.Item
import javax.inject.Inject

class OfferItemViewModel @Inject constructor(private val onClickListener: OnClickListener): ViewModel() {

    val itemBrand = MutableLiveData<String>()
    val itemTitle = MutableLiveData<String>()
    val itemTags = MutableLiveData<String>()
    val itemRating = MutableLiveData<String>()

    fun bind(item: Item, context: Context, imageView: ImageView){
        itemBrand.value = item.brand
        itemTitle.value = item.title
        itemTags.value = item.tags
        itemRating.value = item.favoriteCount.getLikesFormat()

        Glide.with(context)
            .load(item.imageUrl)
            .into(imageView)
    }

    fun onClick(item: Item) {
        onClickListener.onClick(item)
    }
}