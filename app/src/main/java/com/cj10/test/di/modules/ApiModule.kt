package com.cj10.test.di.modules

import com.cj10.test.api.OffersService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
object ApiModule {

    @Provides
    @Singleton
    @JvmStatic
    internal fun provideOffersService(retrofit: Retrofit): OffersService{
        return retrofit.create(OffersService::class.java)
    }
}