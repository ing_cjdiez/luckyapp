package com.cj10.test.views

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cj10.test.common.annotations.Constants
import com.cj10.test.common.errors.NetworkErrorType
import com.cj10.test.common.interfaces.ApiResponse
import com.cj10.test.common.interfaces.ObservableCallback
import com.cj10.test.common.states.ListViewState
import com.cj10.test.models.Offer
import com.cj10.test.repositories.OffersRepository
import io.reactivex.Scheduler
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import javax.inject.Inject
import javax.inject.Named

class OffersViewModel @Inject constructor(
    private val offersRepository: OffersRepository,
    @Named(Constants.UI) private val uiScheduler: Scheduler,
    @Named(Constants.IO) private val ioScheduler: Scheduler
): ViewModel(), ApiResponse<Offer?, Throwable>, ObservableCallback {

    var viewState = MutableLiveData<ListViewState>()

    fun fetchOffers() {
        offersRepository.fetchOffers()
            .subscribeOn(ioScheduler)
            .timeout(10.toLong(), TimeUnit.SECONDS)
            .observeOn(uiScheduler)
            .subscribe(
                { list -> onResponseReceived(list) },
                { error -> onFailure(error) },
                { onFinish() },
                { onStart() }
            )
    }

    override fun onResponseReceived(response: Offer?) {
        viewState.value = if(response!=null) ListViewState.HasSingleData(response) else ListViewState.NoData
    }

    override fun onFailure(error: Throwable) {
        val errorType = if (error is TimeoutException) NetworkErrorType.TIMEOUT else NetworkErrorType.UNKNOWN
        viewState.value = ListViewState.Error(errorType)
    }

    override fun onStart() {
        viewState.value = ListViewState.Loading
    }

    override fun onFinish() {
        viewState.value = ListViewState.FinishLoading
    }
}