package com.cj10.test.common.extensions

const val ONE_THOUSAND = 1000

fun Int.getLikesFormat(): String {
    if (this > ONE_THOUSAND) {
        return "${this.div(ONE_THOUSAND)}k"
    } else {
        return this.toString()
    }
}