package com.cj10.test

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.cj10.test.common.extensions.activityViewModelProvider
import com.cj10.test.common.interfaces.OnClickListener
import com.cj10.test.models.Item
import com.cj10.test.views.OfferDetailsFragment
import com.cj10.test.views.OfferItemDetailsViewModel
import com.cj10.test.views.OffersFragment
import com.cj10.test.views.OffersViewModel
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : AppCompatActivity(), OnClickListener {

    @Inject
    lateinit var modelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        AndroidInjection.inject(this)

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                when(supportFragmentManager.fragments.last()) {
                    is OffersFragment -> finish()
                    else -> showHomeFragment()
                }
            }
        }
        onBackPressedDispatcher.addCallback(this, callback)
        showHomeFragment()
    }

    private fun showHomeFragment() {
        val offersViewModel: OffersViewModel = activityViewModelProvider(modelFactory)
        changeFragment(getOffersFragment(offersViewModel, this))
    }

    private fun getOffersFragment(offersViewModel: OffersViewModel, onClickListener: OnClickListener) =
        OffersFragment.newInstance(offersViewModel, onClickListener)

    private fun getOfferDetailsFragment(offerItemDetailsViewModel: OfferItemDetailsViewModel, item: Item) =
        OfferDetailsFragment.newInstance(offerItemDetailsViewModel, item)

    private fun changeFragment(fragment: Fragment){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_container, fragment, "books")
            .commit()
    }

    override fun onClick(obj: Any) {
        when(obj) {
            is Item -> {
                val offerItemDetailsViewModel: OfferItemDetailsViewModel = activityViewModelProvider(modelFactory)
                changeFragment(getOfferDetailsFragment(offerItemDetailsViewModel, obj))
            }
        }
    }
}