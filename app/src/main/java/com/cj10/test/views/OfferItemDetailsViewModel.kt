package com.cj10.test.views

import android.content.Context
import android.view.View
import android.widget.ImageView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bumptech.glide.Glide
import com.cj10.test.R
import com.cj10.test.common.annotations.Constants
import com.cj10.test.common.errors.NetworkErrorType
import com.cj10.test.common.extensions.getLikesFormat
import com.cj10.test.common.interfaces.ApiResponse
import com.cj10.test.common.interfaces.ObservableCallback
import com.cj10.test.common.states.ListViewState
import com.cj10.test.models.OfferDetails
import com.cj10.test.repositories.OffersRepository
import io.reactivex.Scheduler
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import javax.inject.Inject
import javax.inject.Named

class OfferItemDetailsViewModel @Inject constructor(
    private val offersRepository: OffersRepository,
    @Named(Constants.UI) private val uiScheduler: Scheduler,
    @Named(Constants.IO) private val ioScheduler: Scheduler
): ViewModel(), ApiResponse<OfferDetails?, Throwable>, ObservableCallback {

    var viewState = MutableLiveData<ListViewState>()

    val itemBrand = MutableLiveData<String>()
    val itemTitle = MutableLiveData<String>()
    val itemTags = MutableLiveData<String>()
    val itemRating = MutableLiveData<String>()
    val itemDescription = MutableLiveData<String>()
    val itemExpiration = MutableLiveData<String>()
    val itemPriceOld = MutableLiveData<String>()
    val itemPriceNew = MutableLiveData<String>()
    val itemRedemption = MutableLiveData<String>()

    fun getOfferDetailsInformation(url: String) {
        offersRepository.getOfferDetails(url)
            .subscribeOn(ioScheduler)
            .timeout(10.toLong(), TimeUnit.SECONDS)
            .observeOn(uiScheduler)
            .subscribe(
                { list -> onResponseReceived(list) },
                { error -> onFailure(error) },
                { onFinish() },
                { onStart() }
            )
    }

    fun bind(details: OfferDetails, context: Context, imageView: ImageView){

        itemDescription.value = details.description
        itemBrand.value = details.brand
        itemTitle.value = details.title
        itemTags.value = details.tags
        itemExpiration.value = details.expiration
        itemPriceOld.value = details.price.old
        itemPriceNew.value = details.price.new
        itemRedemption.value = context.getString(R.string.redemptions_cap_x, details.redemptionsCap)
        itemRating.value = details.favoriteCount.getLikesFormat()

        Glide.with(context)
            .load(details.imageUrl)
            .into(imageView)
    }

    override fun onResponseReceived(response: OfferDetails?) {
        viewState.value = if (response!=null) ListViewState.HasSingleData(response) else ListViewState.NoData
    }

    override fun onFailure(error: Throwable) {
        val errorType = if (error is TimeoutException) NetworkErrorType.TIMEOUT else NetworkErrorType.UNKNOWN
        viewState.value = ListViewState.Error(errorType)
    }

    override fun onStart() {
        viewState.value = ListViewState.Loading
    }

    override fun onFinish() {
        viewState.value = ListViewState.FinishLoading
    }
}