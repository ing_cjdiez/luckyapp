package com.cj10.test.repositories

import com.cj10.test.api.OffersService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OffersRepository @Inject constructor(private val offersService: OffersService) {
    fun fetchOffers() = offersService.getAllOffers()
    fun getOfferDetails(url: String) = offersService.getOfferDetails(url)
}