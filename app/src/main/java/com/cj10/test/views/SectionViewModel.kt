package com.cj10.test.views

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cj10.test.common.states.ListViewState
import com.cj10.test.models.Section
import javax.inject.Inject

class SectionViewModel @Inject constructor(): ViewModel() {

    var viewState = MutableLiveData<ListViewState>()
    val sectionTitle = MutableLiveData<String>()

    fun bind(section: Section) {
        sectionTitle.value = section.title
        viewState.value = ListViewState.HasData(section.items)
    }
}