package com.cj10.test.models

import com.squareup.moshi.Json

data class Price(
    @Json(name = "old") val old: String,
    @Json(name = "new") val new: String
)