package com.cj10.test.models

import com.squareup.moshi.Json

data class Item(
    @Json(name = "detailUrl") val detailUrl: String,
    @Json(name = "imageUrl") val imageUrl: String,
    @Json(name = "brand") val brand: String,
    @Json(name = "title") val title: String,
    @Json(name = "tags") val tags: String,
    @Json(name = "favoriteCount") val favoriteCount: Int
)