package com.cj10.test.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.cj10.test.R
import com.cj10.test.common.interfaces.OnStateChange
import com.cj10.test.databinding.SectionLayoutBinding
import com.cj10.test.models.Section

class SectionAdapter(
    private val owner: LifecycleOwner,
    private val onStateChange: OnStateChange
): RecyclerView.Adapter<SectionAdapter.ViewHolder>() {

    private var sectionsList: ArrayList<Section> = ArrayList()
    private lateinit var binding: SectionLayoutBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.section_layout,
            parent,
false
        )
        return ViewHolder(binding, owner, onStateChange)
    }

    override fun getItemCount() = sectionsList.size

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.bind(sectionsList[pos])
    }

    fun updateSections(sections: List<Section>) {
        sectionsList.clear()
        sectionsList.addAll(sections)
        notifyDataSetChanged()
    }

    class ViewHolder(
        private val sectionLayoutBinding: SectionLayoutBinding,
        private val owner: LifecycleOwner,
        private val onStateChange: OnStateChange
    ) : RecyclerView.ViewHolder(sectionLayoutBinding.root) {

        private val viewModel = SectionViewModel()

        fun bind(section: Section) {
            viewModel.bind(section)
            sectionLayoutBinding.viewModel = viewModel
            viewModel.viewState.observe(owner, Observer {
                onStateChange.onChange(it, sectionLayoutBinding.recyclerviewSectionItems)
            })
        }
    }
}