package com.cj10.test.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cj10.test.common.annotations.ViewModelKey
import com.cj10.test.di.factory.ViewModelFactory
import com.cj10.test.views.OfferItemDetailsViewModel
import com.cj10.test.views.OffersViewModel
import com.cj10.test.views.SectionViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(OffersViewModel::class)
    internal abstract fun bindOffersViewModel(viewModel: OffersViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OfferItemDetailsViewModel::class)
    internal abstract fun bindOfferItemDetailsViewModel(viewModel: OfferItemDetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SectionViewModel::class)
    internal abstract fun bindSectionViewModel(viewModel: SectionViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}