package com.cj10.test.models

import com.squareup.moshi.Json

data class Section(
    @Json(name = "title") val title: String,
    @Json(name = "items") val items: List<Item>
)