package com.cj10.test.views

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cj10.test.R
import com.cj10.test.common.interfaces.OnClickListener
import com.cj10.test.common.interfaces.OnStateChange
import com.cj10.test.common.states.ListViewState
import com.cj10.test.models.Item
import com.cj10.test.models.Offer
import kotlinx.android.synthetic.main.offers_layout.view.recyclerview_sections
import kotlinx.android.synthetic.main.offers_layout.view.tv_loading_offers

class OffersFragment(private val viewModel: OffersViewModel, onClickListener: OnClickListener): Fragment(), OnStateChange {

    private lateinit var binding: View
    private val sectionsAdapter = SectionAdapter(this, this)
    private val itemsAdapter = ItemsAdapter(onClickListener)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.viewState.observe(this, Observer { handleStateChange(it) })
        viewModel.fetchOffers()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = inflater.inflate(R.layout.offers_layout, container, false)

        activity?.title = getString(R.string.app_name)

        binding.recyclerview_sections.apply {
            adapter = sectionsAdapter
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                    context,
                    DividerItemDecoration.VERTICAL
                )
            )
        }

        //setHasOptionsMenu(true)

        return binding
    }

    /*
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_help, menu);
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_help_item -> {
                onClickListener.onClick(Unit)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
    */

    private fun handleStateChange(state: ListViewState?) {
        when (state) {
            is ListViewState.Loading -> binding.apply {
                tv_loading_offers.visibility = View.VISIBLE
                binding.recyclerview_sections.visibility = View.GONE
            }
            is ListViewState.HasSingleData<*> -> {
                binding.apply {
                    tv_loading_offers.visibility = View.GONE
                    recyclerview_sections.visibility = View.VISIBLE
                }
                sectionsAdapter.updateSections((state as ListViewState.HasSingleData<Offer>).data.sections)
            }
            else -> binding.apply {
                tv_loading_offers.visibility = View.GONE
                binding.recyclerview_sections.visibility = View.VISIBLE
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(
            offersViewModel: OffersViewModel,
            onClickListener: OnClickListener
        ): OffersFragment {
            return OffersFragment(offersViewModel, onClickListener)
        }
    }

    override fun onChange(state: ListViewState?, recyclerView: RecyclerView) {
        when (state) {
            is ListViewState.HasData<*> -> {
                recyclerView.apply {
                    adapter = itemsAdapter
                    layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                    addItemDecoration(
                        DividerItemDecoration(
                            context,
                            DividerItemDecoration.VERTICAL
                        )
                    )
                }
                binding.tv_loading_offers.visibility = View.GONE
                recyclerView.visibility = View.VISIBLE
                itemsAdapter.updateItems((state as ListViewState.HasData<Item>).dataList)
            }
            is ListViewState.Loading -> {
                binding.tv_loading_offers.visibility = View.VISIBLE
                recyclerView.visibility = View.GONE
            }
            else -> {
                binding.tv_loading_offers.visibility = View.GONE
                recyclerView.visibility = View.VISIBLE
            }
        }
    }
}