package com.cj10.test.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cj10.test.R
import com.cj10.test.common.interfaces.OnClickListener
import com.cj10.test.databinding.ItemLayoutBinding
import com.cj10.test.models.Item

class ItemsAdapter(private val onClickListener: OnClickListener): RecyclerView.Adapter<ItemsAdapter.ViewHolder>() {

    private lateinit var binding: ItemLayoutBinding
    var bookmarksList: ArrayList<Item> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_layout,
            parent,
false
        )
        return ViewHolder(binding, onClickListener)
    }

    override fun getItemCount() = bookmarksList.size

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.bind(bookmarksList[pos])
    }

    fun updateItems(items: List<Item>) {
        bookmarksList.clear()
        bookmarksList.addAll(items)
        notifyDataSetChanged()
    }

    class ViewHolder(
        private val itemLayoutBinding: ItemLayoutBinding,
        onClickListener: OnClickListener
    ) : RecyclerView.ViewHolder(itemLayoutBinding.root) {

        private val viewModel = OfferItemViewModel(onClickListener)

        fun bind(item: Item) {
            viewModel.bind(item, itemView.context, itemLayoutBinding.imageViewProduct)
            itemLayoutBinding.viewModel = viewModel
            itemLayoutBinding.item = item
        }
    }
}