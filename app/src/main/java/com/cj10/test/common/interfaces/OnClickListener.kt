package com.cj10.test.common.interfaces

interface OnClickListener {
    fun onClick(obj: Any)
}