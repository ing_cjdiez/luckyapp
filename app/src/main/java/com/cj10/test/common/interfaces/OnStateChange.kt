package com.cj10.test.common.interfaces

import androidx.recyclerview.widget.RecyclerView
import com.cj10.test.common.states.ListViewState

interface OnStateChange {
    fun onChange(state: ListViewState?, recyclerView: RecyclerView)
}