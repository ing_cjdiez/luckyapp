package com.cj10.test.api

import com.cj10.test.BuildConfig
import com.cj10.test.models.Offer
import com.cj10.test.models.OfferDetails
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface OffersService {
    @GET(BuildConfig.OFFERS_ENDPOINT)
    fun getAllOffers(): Observable<Offer>

    @GET("{fullUrl}")
    fun getOfferDetails(@Path(value = "fullUrl", encoded = true) fullUrl: String?): Observable<OfferDetails>
}
