package com.cj10.test.models

import com.squareup.moshi.Json

data class Offer(
    @Json(name = "title") val title: String,
    @Json(name = "sections") val sections: List<Section>
)