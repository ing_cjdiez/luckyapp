package com.cj10.test.models

import com.squareup.moshi.Json

data class OfferDetails(
    @Json(name = "id") val id: Int,
    @Json(name = "imageUrl") val imageUrl: String,
    @Json(name = "brand") val brand: String,
    @Json(name = "title") val title: String,
    @Json(name = "tags") val tags: String,
    @Json(name = "favoriteCount") val favoriteCount: Int,
    @Json(name = "description") val description: String,
    @Json(name = "expiration") val expiration: String,
    @Json(name = "redemptionsCap") val redemptionsCap: String,
    @Json(name = "price") val price: Price
)